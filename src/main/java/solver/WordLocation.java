package solver;

/**
 * Class that represents a pair of {Coordinates}
 */
public class WordLocation {

    private Coordinate startCoordinate;
    private Coordinate endCoordinate;

    public WordLocation(Coordinate startCoordinate, Coordinate endCoordinate) {
        this.startCoordinate = startCoordinate;
        this.endCoordinate = endCoordinate;
    }

    public Coordinate getStartCoordinate() {
        return startCoordinate;
    }

    public Coordinate getEndCoordinate() {
        return endCoordinate;
    }
}
