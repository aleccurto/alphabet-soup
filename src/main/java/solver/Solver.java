package solver;

import data.Board;
import java.util.Optional;
import java.util.function.Consumer;

/**
 * Class to help solve a word search.
 */
public class Solver {

    private final Board board;

    public Solver(Board board){
        this.board = board;
    }

    /**
     * Find the start and end coordinates for the given word.
     * when found pass to the consumer
     * @param word
     * @param consumer
     */
    public void findWord(String word, Consumer<WordLocation> consumer){
        //This method is a bit cumbersome, but it gets the job done.
        //Gorss that there are 4 nested for-loops.
        for(int row = 0; row <  this.board.getRows(); row++){
            for(int col = 0; col <  this.board.getColumns(); col++){
                //first we need to find the first letter of our word.
                Optional<Character> charAt = this.board.getCharAt(row, col);
                if(charAt.isPresent()) {
                    if (charAt.get().charValue() == word.charAt(0)) {
                        //we have a potential starting location, lets check it now.
                        //we have 8 directions to pick from Up, Down, Left, Right, (4) Diagonals.
                        //we can loop through these to see if the word starts here or not.
                        for(int rowMod = -1; rowMod <= 1; rowMod++){
                            for(int colMod = -1; colMod <= 1; colMod++){
                                if(rowMod == 0 && colMod ==0){
                                    continue; //We can skip this as it will always be the same char.
                                }
                                Optional<Coordinate> coordinate = tryDirection(row+rowMod, col+colMod, rowMod, colMod, word, 1);
                                if(coordinate.isPresent()){
                                    // we found the end point. lets populate our return value now.
                                    Coordinate startCoordinate = new Coordinate(row, col);
                                    Coordinate endCoordinate = coordinate.get();
                                    WordLocation location = new WordLocation(startCoordinate, endCoordinate);
                                    consumer.accept(location);
                                    return;
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * Search for the given word recursively using the starting row/col pair. Then follow the direction specified by the
     * rowMod/ColMod
     * @param row Which row to search on.
     * @param col Which column to search on.
     * @param rowMod Specifies how to iterate over rows to search direction. Value should be one of [-1,0,1]
     * @param colMod Specifies how to iterate over columns to search direction. Value should be one of [-1,0,1]
     * @param word The word being searched for
     * @param wordIndex what letter in the word is currently being searched for.
     * @return
     */
    private Optional<Coordinate> tryDirection(int row, int col, int rowMod, int colMod, String word, int wordIndex){
        if(wordIndex == word.length()-1){
            //we are checking the last character (almost done)
            Optional<Character> charAt = this.board.getCharAt(row, col);
            if(charAt.isPresent()) {
                if (charAt.get().charValue() == word.charAt(wordIndex)) {
                    Coordinate coordinate = new Coordinate(row, col);
                    return Optional.of(coordinate);
                }
            }
        }
        Optional<Character> charAt = this.board.getCharAt(row, col);
        if(charAt.isPresent()){
            if (charAt.get().charValue() == word.charAt(wordIndex)){
                Optional<Coordinate> coordinate = tryDirection(row + rowMod, col + colMod, rowMod, colMod, word, wordIndex + 1);
                return coordinate;
            }
        }
        return Optional.empty();
    }


}
