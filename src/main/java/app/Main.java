package app;

import data.Board;
import solver.Solver;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String args[]){

        if(args.length > 0){
            //Read in the file and build up the needed objects
            String fileName = args[0];
            Path path = FileSystems.getDefault().getPath(fileName);
            try {
                List<String> lines = Files.readAllLines(path);
                int numRows = 0;
                int numColumns = 0;
                List<String> gameRows = new ArrayList<>();
                List<String> wordsList = new ArrayList<>();
                for (int i = 0; i < lines.size() ; i++) {
                    if(i == 0){
                        //set up grid size.
                        String s = lines.get(i);
                        String[] size = s.split("x");
                        numRows = Integer.parseInt(size[0]);
                        numColumns = Integer.parseInt(size[1]);
                    }else if(i <= numRows ){
                        //we are offset by 1, since 1st line is size of grid
                        gameRows.add(lines.get(i).replace(" ", ""));
                    }else{
                        // now we find words.
                        wordsList.add(lines.get(i));
                    }
                }

                //build the needed objects.
                Board board = new Board(numRows, numColumns, gameRows);
                Solver solver = new Solver(board);

                //setup complete, now find some words.
                for (String word : wordsList ) {
                    solver.findWord(word, wordLocation -> {
                        //this is the output for the challenge
                        System.out.println(word + " " + wordLocation.getStartCoordinate().toString() + " " + wordLocation.getEndCoordinate().toString());
                    });
                }

            } catch (IOException e) {
                //couldn't find file
                e.printStackTrace();
            }

        }
    }
}
