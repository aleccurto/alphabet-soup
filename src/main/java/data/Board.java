package data;

import java.util.List;
import java.util.Optional;

/**
 * Class that represents the game board.
 */
public class Board {
    private int rows;
    private int columns;
    private List<String> gameRows;

    public Board(int rows, int columns, List<String> gameRows) {
        this.rows = rows;
        this.columns = columns;
        this.gameRows = gameRows;
    }

    public int getRows() {
        return rows;
    }

    public int getColumns() {
        return columns;
    }

    public List<String> getGameRows() {
        return gameRows;
    }

    /**
     * Get the character located at the given row/col pair
     * @param row
     * @param col
     * @return Returns the character at the located coordinate, wrapped in an Optional wrapper. Will be empty if row or
     * col was out of bounds.
     */
    public Optional<Character> getCharAt(int row, int col){
        if(row >= 0 && row < gameRows.size()){
            String rowString = gameRows.get(row);
            if(col >= 0 && col < rowString.length()){
                Optional<Character> character = Optional.of(rowString.charAt(col));
                return character;
            }else{
                return Optional.empty();
            }
        }else{
            return Optional.empty();
        }
    }
}
